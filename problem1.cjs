const fs = require("fs");

function createDirectory(path) {
  return new Promise((resolve, reject) => {
    fs.mkdir(path, (err) => {
      if (err) {
        reject("Error occured while creating directory!");
      } else {
        resolve(`Directory is created successfully!`);
      }
    });
  });
}

function createFile(path) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, `"hi"`, "utf-8", (error) => {
      if (error) {
        reject("Error occured while creating file!");
      } else {
        resolve("File is creted successfully!");
      }
    });
  });
}

function deleteFile(path) {
  return new Promise((resolve, reject) => {
    fs.unlink(path, (error) => {
      if (error) {
        reject("File is deleted successfully!");
      } else {
        resolve("The file is deleted successfully!");
      }
    });
  });
}

function doAllOperations() {
  createDirectory("random-directory")
    .then((result) => console.log(result))
    .then(() => {
      let store_create_promises = [];
      for (let file_number = 1; file_number <= 5; file_number++) {
        const path = `random-directory/file-${file_number}.json`;
        store_create_promises.push(createFile(path));
      }
      return store_create_promises;
    })
    .then((result) => {
      return Promise.all(result);
    })
    .then((result) => {
      console.log(result);
    })
    .then(() => {
      let store_delete_promises = [];
      for (let file_number = 1; file_number <= 5; file_number++) {
        const path = `random-directory/file-${file_number}.json`;
        store_delete_promises.push(deleteFile(path));
      }
      return store_delete_promises;
    })
    .then((result) => {
      return Promise.all(result);
    })
    .then((result) => {
      console.log(result);
    })
    .catch((error) => console.log(error));
}

module.exports = doAllOperations;