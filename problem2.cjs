const fs = require("fs");

function readFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, "utf-8", (error, data) => {
      if (error) {
        reject("Error occured while reading file!!");
      } else {
        resolve(data);
      }
    });
  });
}

function writeFile(path, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, (error) => {
      if (error) {
        reject("Error occured while creating file!");
      } else {
        resolve(`File ${path} is created successfully!`);
      }
    });
  });
}

function deleteFile(path) {
  return new Promise((resolve, reject) => {
    fs.unlink(path, (error) => {
      if (error) {
        reject("Error occured while deleting file");
      } else {
        resolve("File is deleted successfully!!");
      }
    });
  });
}

function appendFile(path, data) {
  return new Promise((resolve, reject) => {
    fs.appendFile(path, data, (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve(`${path} is updated successfully!`);
      }
    });
  });
}

function doAllOperation() {
  readFile('./data/lipsum_1.txt')
    .then((result) => {
      return result;
    })
    .then((result) => {
      const store_data = result.toUpperCase();
      return writeFile("upperCaseData.txt", store_data);
    })
    .then((result) => {
      console.log(result);
      return writeFile("filenames.txt", `upperCaseData.txt`);
    })
    .then((result) => {
      console.log(result);
      return readFile("upperCaseData.txt");
    })
    .then((result) => {
      let store_data = result.toLowerCase();
      return store_data.split(".");
    })
    .then((result) => {
      return writeFile("splittedData.txt", JSON.stringify(result));
    })
    .then((result) => {
      console.log(result);
      return appendFile("filenames.txt", `\nsplittedData.txt`);
    })
    .then((result) => {
      console.log(result);
      return readFile("splittedData.txt");
    })
    .then((result) => {
      const storeSortedData = JSON.parse(result).sort();
      return writeFile("sortedData.txt", JSON.stringify(storeSortedData));
    })
    .then((result) => {
      console.log(result);
      return appendFile("filenames.txt", `\nsortedData.txt`);
    })
    .then((result) => {
      console.log(result);
      return readFile("filenames.txt");
    })
    .then((result) => {
      const split_string = result.split("\n");
      return split_string;
    })
    .then((result) => {
      let deleteAllFiles = [];
      for (let index = 0; index < result.length; index++) {
        deleteAllFiles.push(deleteFile(result[index]));
      }
      return Promise.all(deleteAllFiles);
    })
    .then((result) => {
      console.log(result);
    })
    .catch((error) => {
      console.log(error);
    });
}

module.exports = doAllOperation;
